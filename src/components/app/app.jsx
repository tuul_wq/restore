import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from '../routes/routes';
import Header from '../header/header';

function App() {
  return (
    <main className="container">
      <Router>
        <Header />
        <Routes />
      </Router>
    </main>
  )
}

export default App;
