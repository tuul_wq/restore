import React from 'react';
import './error-indicator.scss';

function ErrorIndicator({ status = 'Oops', message = 'Something went wrong!' }) {
  return (
    <div className="error-indicator">
      <div className="error-status">{status}</div>
      <div className="error-message">{message}</div>
    </div>
  )
}

export default ErrorIndicator;
