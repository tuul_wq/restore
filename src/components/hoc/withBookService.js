import React from 'react';
import { BookServiceConsumer } from '../service-context/book-service.context';

function withBookService(Component) {
  return function(props) {
    return (
      <BookServiceConsumer>
        {
          service => <Component { ...props } service={service}/>
        }
      </BookServiceConsumer>
    );
  }
}

export default withBookService;