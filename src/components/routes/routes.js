import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from '../../pages/home/home';
import Cart from '../../pages/cart/cart';

function Routes() {
  return (
    <Switch>
      <Route path="/" exact>
        <Redirect to="/home" />
      </Route>
      <Route path="/home" exact component={Home}/>
      <Route path="/cart" exact component={Cart}/>
      <Route component={() => <div>404</div>}/>
    </Switch>
  )
}

export default Routes;
