import React, { Component } from 'react';
import { connect } from 'react-redux';
import BookListItem from '../book-list-item/book-list-item';
import { fetchBooks, bookAddedToCart } from '../../store/actions';
import withBookService from '../../components/hoc/withBookService';
import Spinner from '../../components/spinner/spinner';
import ErrorIndicator from '../../components/error-indicator/error-indicator';
import { compose } from '../../utils/compose';
import './book-list.scss';

class BookListContainer extends Component {
  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const { books, loading, error, onBookAdded } = this.props;
    if (loading) return <Spinner />;
    if (error) return <ErrorIndicator {...error} />;

    return (
      <BookList books={books} onBookAdded={onBookAdded} />
    );
  }
}

function BookList({ books, onBookAdded }) {
  return (
    <ul className="book-list">
      {books.map(({ id, ...book }) =>
        <li key={id}>
          <BookListItem book={book} onBookAdded={() => onBookAdded(id)} />
        </li>
      )}
    </ul>
  );
}

const mapStateToProps = (state) => {
  return {
    books: state.books,
    loading: state.loading,
    error: state.error,
  }
}

const mapDispatchToProps = (dispatch, { service }) => {
  return {
    fetchBooks: fetchBooks(service, dispatch),
    onBookAdded: (id) => dispatch(bookAddedToCart(id))
  }
}

export default compose(
  withBookService,
  connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
