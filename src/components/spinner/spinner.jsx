import React from 'react';
import spinner from '../../assets/images/spinner.svg';
import './spinner.scss';

function Spinner() {
  return (
    <div className="spinner-container">
      <img className="spinner" src={spinner} alt="spinner"/>
    </div>
  )
}

export default Spinner;
