import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './components/app/app';
import store from './store/store';
import ErrorBoundry from './components/error-boundry/error-boundry';
import BookStoreService from './services/book-store.service';
import { BookServiceProvider } from './components/service-context/book-service.context';
import './assets/styles/index.scss';

const service = new BookStoreService();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ErrorBoundry>
        <BookServiceProvider value={service}>
          <App />
        </BookServiceProvider>
      </ErrorBoundry>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
