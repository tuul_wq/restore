import React from 'react';
import BookList from '../../components/book-list/book-list';
import CartTable from '../../components/cart-table/cart-table';

function Home() {
  return (
    <>
      <BookList />
      <CartTable />
    </>
  );
}

export default Home;
