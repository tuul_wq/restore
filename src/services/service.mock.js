export const GET_BOOKS = [
  {
    id: 'sdsa',
    title: 'Great developer',
    author: 'Mijgan The One',
    price: 25.75,
    coverImage: 'http://covers.openlibrary.org/b/id/8406786-M.jpg'
  },
  {
    id: '43tds',
    title: 'React is cool',
    author: 'Terentij',
    price: 15.45,
    coverImage: 'http://covers.openlibrary.org/b/id/8406785-M.jpg'
  },
  {
    id: '9sdfu',
    title: 'More books here',
    author: 'Starik Hotabich',
    price: 85.00,
    coverImage: 'http://covers.openlibrary.org/b/id/8806785-M.jpg'
  },
  {
    id: 'biobjdf',
    title: 'Best way to be a hero',
    author: 'Ivanov G.G.',
    price: 37.15,
    coverImage: 'http://covers.openlibrary.org/b/id/8506785-M.jpg'
  },
  {
    id: 'dnsf2',
    title: 'New job is coming!',
    author: 'Pamelo A.S.',
    price: 65.50,
    coverImage: 'http://covers.openlibrary.org/b/id/8106785-M.jpg'
  },
];
