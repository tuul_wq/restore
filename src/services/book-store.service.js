import { GET_BOOKS } from './service.mock';

class BookStoreService {

  getBooks() {
    return fetchMock(GET_BOOKS);
  }
}

function fetchMock(mock) {
  return new Promise((resolve, reject) => {
    if (Math.random() > 0.8) {
      setTimeout(() => reject({ status: 'Failed', message: 'Error occured' }), 500);
    } else {
      setTimeout(() => resolve(mock), 500);
    }
  });
}

export default BookStoreService;