function booksLoaded(newBooks) {
  return {
    type: 'FETCH_BOOKS_SUCCESS',
    payload: newBooks
  }
}

function booksRequested() {
  return {
    type: 'FETCH_BOOKS_REQUEST'
  }
}

function booksError(error) {
  return {
    type: 'FETCH_BOOKS_FAILURE',
    payload: error
  }
}

export function bookAddedToCart(id) {
  return {
    type: 'BOOK_ADDED_TO_CART',
    payload: id
  }
}

export function bookRemovedFromCart(id) {
  return {
    type: 'BOOK_REMOVED_FROM_CART',
    payload: id
  }
}

export function bookDecreaseInCart(id) {
  return {
    type: 'BOOK_DECREASE_IN_CART',
    payload: id
  }
}

export function fetchBooks(service, dispatch) {
  return async function() {
    dispatch(booksRequested());
    try {
      const books = await service.getBooks();
      dispatch(booksLoaded(books));
    } catch (error) {
      dispatch(booksError(error));
    }
  }
}
