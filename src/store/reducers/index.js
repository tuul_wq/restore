const initialState = {
  books: [],
  loading: true,
  error: null,
  cartItems: [],
  orderTotal: 225
};

const reducer = (state = initialState, { payload, type }) => {
  switch(type) {
    case 'FETCH_BOOKS_REQUEST':
      return {
        ...state,
        books: [],
        loading: true,
        error: null
      };
    case 'FETCH_BOOKS_SUCCESS':
      return {
        ...state,
        books: payload,
        loading: false,
        error: null
      };
    case 'FETCH_BOOKS_FAILURE':
      return {
        ...state,
        books: [],
        loading: false,
        error: payload
      };
    case 'BOOK_ADDED_TO_CART':
      return updateCart(state, payload, 1);
    case 'BOOK_REMOVED_FROM_CART':
      const item = state.cartItems.find(cartBook => cartBook.id === payload);
      return updateCart(state, payload, -item.count);
    case 'BOOK_DECREASE_IN_CART':
      return updateCart(state, payload, -1);
    default:
      return state;
  }
};

function updateCart(state, bookId, quantity) {
  const { books, cartItems } = state;
  const book = books.find(book => book.id === bookId);
  const cartBookIndex = cartItems.findIndex(cartBook => cartBook.id === bookId);
  const cartBook = cartItems[cartBookIndex];

  const newBook = makeNewBook(cartBook, book, quantity);
  return {
    ...state,
    cartItems: updateCartItems(cartItems, cartBookIndex, newBook)
  };
}

function makeNewBook(item = {}, book, quantity) {
  const {
    id = book.id,
    title = book.title,
    count = 0,
    total = 0
  } = item;

  return {
    id,
    title,
    count: count + quantity,
    total: total + quantity * book.price
  }
}

function updateCartItems(items, index, book) {
  if (book.count === 0) return items.filter(cartBook => cartBook.id !== book.id);

  if (index === -1) {
    return [...items, book];
  }
  return items.map((cartBook, idx) => idx === index ? book : cartBook );
}

export default reducer;