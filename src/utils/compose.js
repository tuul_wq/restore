export function compose(...funcs) {
  return function(component) {
    return funcs.reduceRight((acc, fn) => fn(acc), component);
  }
}
